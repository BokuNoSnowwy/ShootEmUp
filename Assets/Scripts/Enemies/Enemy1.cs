﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : EnemyBehaviors
{
    // Start is called before the first frame update
    void Start()
    {
        AttributeValues();
    }

    // Update is called once per frame
    void Update()
    {
        ShootTimer();

        transform.Translate(Vector3.down * Time.deltaTime * speed);
        //transform.Translate(Vector3.right * Mathf.Sin(Time.deltaTime));
    }
    
        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Debug.Log("Touch");
                other.GetComponent<PlayerStats>().TakeDamage(10);
    
                Destroy(gameObject);
            }
        }
}
