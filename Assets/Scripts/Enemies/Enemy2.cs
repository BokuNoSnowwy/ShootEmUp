﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : EnemyBehaviors
{
    private bool goRight = true;
    // Start is called before the first frame update
    void Start()
    {
        AttributeValues();
    }

    // Update is called once per frame
    void Update()
    {
        
        ShootTimer();
        
        if (goRight)
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
        else
        {
            transform.Translate(-Vector3.right * Time.deltaTime * speed);
        }

        if (transform.position.x + GetComponent<BoxCollider2D>().size.x > screenDimensions.x)
        {
            goRight = false;
        }else if (transform.position.x - GetComponent<BoxCollider2D>().size.x < -screenDimensions.x )
        {
            goRight = true;
        }
    }
    
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Touch");
            other.GetComponent<PlayerStats>().TakeDamage(10);

            Destroy(gameObject);
        }
    }
}
