﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class EnemyBehaviors : MonoBehaviour
{
    protected Camera camera;
    [SerializeField] protected Vector3 screenDimensions;
    [SerializeField] protected float speed;

    
    [Header("Shoot Management")]
    [SerializeField] protected Transform shootSpot;
    [SerializeField] protected GameObject bullet;
    [SerializeField] protected float timeBeforeShooting;
    private float maxTimeBeforeShooting;

    // Start is called before the first frame update
    protected void Start()
    {
        AttributeValues();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    protected void AttributeValues()
    {
        maxTimeBeforeShooting = timeBeforeShooting;
        camera = Camera.main;
        screenDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,0));
    }

    protected void Shoot()
    {
        Instantiate(bullet, shootSpot.position, quaternion.identity);
    }

    protected void ShootTimer()
    {
        if (timeBeforeShooting != -1)
        {
            timeBeforeShooting -= Time.deltaTime;
            if (timeBeforeShooting <= 0) 
            {
                Shoot(); 
                timeBeforeShooting = maxTimeBeforeShooting; 
            }
        }
    }
}
