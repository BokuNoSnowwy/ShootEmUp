﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBoss1 : EnemyBehaviors
{

    [SerializeField] private bool isSetup;
    [SerializeField] private bool goRight = true;

    [SerializeField] private List<Transform> additionnalShootSpot = new List<Transform>();

    [SerializeField] private float timeBeforeShootingBoss;
    [SerializeField] private float maxTimeBeforeShootingBoss;
    private EnemyLife lifeBoss;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SetupBoss());
        AttributeValues();
        lifeBoss = GetComponent<EnemyLife>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isSetup)
        {
            if (goRight)
            {
                transform.Translate(Vector3.right * Time.deltaTime * speed);
            }
            else
            {
                transform.Translate(-Vector3.right * Time.deltaTime * speed);
            }
            
            if (transform.position.x + GetComponent<BoxCollider2D>().size.x > screenDimensions.x)
            {
                goRight = false;
            }
            else if (transform.position.x - GetComponent<BoxCollider2D>().size.x < -screenDimensions.x )
            {
                goRight = true;
            }

            if (lifeBoss.lifeCurrent <= 50)
            {
                ShootBoss();
                ShootTimer();
                speed = 2.5f;
            }
            else
            {
                ShootTimer();
            }


        }
        else
        {
            transform.Translate(Vector3.down * Time.deltaTime * speed/3);
        }

        if (lifeBoss.lifeCurrent <= 0)
        {
            GameplayManager.Instance.Victory();
        }

        //transform.Translate(Vector3.right * Mathf.Sin(Time.deltaTime));
    }
    
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Touch");
            other.GetComponent<PlayerStats>().TakeDamage(10);
        }
    }
    
    void ShootBoss()
    {
        if (timeBeforeShootingBoss != -1)
        {
            timeBeforeShootingBoss -= Time.deltaTime;
            if (timeBeforeShootingBoss <= 0) 
            {
                foreach (var spot in additionnalShootSpot)
                {
                    CircleShot(spot);
                }
                timeBeforeShootingBoss = maxTimeBeforeShootingBoss; 
            }
        }
    }

    void CircleShot(Transform pos)
    {
        for (int i = 0; i < 5; i++)
        {
            Instantiate(bullet, pos.position,Quaternion.Euler(Quaternion.identity.x,Quaternion.identity.y,-90 + i * 45));
        }
    }

    IEnumerator SetupBoss()
    {
        yield return new WaitForSeconds(3);
        isSetup = true;
    }
}
