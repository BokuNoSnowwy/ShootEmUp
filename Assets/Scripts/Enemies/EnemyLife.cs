﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

public class EnemyLife : MonoBehaviour
{
    public int lifeCurrent;
    public int scorePoints;

    void Update()
    {
        if (lifeCurrent <= 0)
        {
            Death();
        }
    }

    public void Hurt(int degats)
    {
        lifeCurrent -= degats;
    }

    void Death()
    {
        if (GetComponent<Animator>())
        {
            GetComponent<Animator>().SetTrigger("Death");
        }
        FindObjectOfType<AudioManager>().Play("Explosion");
    }

    public void Destroy()
    {
        GameplayManager.Instance.IncrementScore(scorePoints);
        Destroy(gameObject);
    }
}
