﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private float timerSpawn;
    private float timerSpawnMax;
    [SerializeField] private float offsetYSpawn;

    [SerializeField] private List<GameObject> listPrefab = new List<GameObject>();
    [SerializeField] private GameObject bossPrefabLevel;

    [SerializeField] private GameObject shipList;
    [SerializeField] private bool bossHasSpawned;

    private Camera camera;
    [SerializeField] protected Vector3 screenDimensions;
    
    // Start is called before the first frame update
    void Start()
    {
        timerSpawnMax = timerSpawn;
        camera = Camera.main;
        screenDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height,0));
    }

    // Update is called once per frame
    void Update()
    {
        if (!bossHasSpawned)
        {
            timerSpawn -= Time.deltaTime;
            if (timerSpawn <= 0)
            {
                SpawnEnemy();
                timerSpawn = timerSpawnMax;
            }
        }
    }

    void SpawnEnemy()
    {
        Vector3 randomPos = new Vector3(Random.Range(- screenDimensions.x + 1f,screenDimensions.x - 1f),transform.position.y - offsetYSpawn,0);
        GameObject enemy = Instantiate(listPrefab[Random.Range(0, listPrefab.Count)], randomPos, Quaternion.identity);
        enemy.transform.parent = shipList.transform;
    }

    public void SpawnBoss()
    {
        if (!bossHasSpawned)
        {
            for (int i = 0; i < shipList.transform.childCount; i++)
            {
                Destroy(shipList.transform.GetChild(i).gameObject);
            }

            Vector3 posBoss = camera.ViewportToWorldPoint(new Vector3(0.5f, 1f, 0));
            Instantiate(bossPrefabLevel, new Vector3(posBoss.x,posBoss.y,0),quaternion.identity);
            bossHasSpawned = true;
        }
    }
}
