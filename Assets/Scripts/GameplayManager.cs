﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    [Header("Score")]
    public int playerScore;
    public TMP_Text scoreText;
    
    [Header("Pause")]
    private bool _isPaused;
    public GameObject pauseMenu;


    
    [Header("Victory")] 
    public int enemyCounter;
    public GameObject victoryMenu;
    public TMP_Text endScoreText;
    
    [Header("Defeat")]
    public GameObject gameOverMenu;
    public TMP_Text gameOverScoreText;
    public static GameplayManager Instance;
    
    [SerializeField] private Transform respawnPoint;


    private bool gameHasEnded = false;
    public EnemySpawner enemyspawner;

    [Header("Player UI")]
    public GameObject player;
    public TMP_Text lifeText;
    public TMP_Text pvText;

    private int lifePlayer = 3;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        respawnPoint = GameObject.FindWithTag("Respawn").transform;
        player = GameObject.FindWithTag("Player");

        UpdateLifePlayer();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel") && !gameHasEnded)
        {
            if (!_isPaused)
            {
                Pause();
            }
            else
            {
                Resume();
            }
        }

        if (enemyCounter <= 0)
        {
            SpawnBoss();
        }
        
    }

    public void IncrementScore(int points)
    {
        playerScore += points;
        scoreText.text = $"Score : {playerScore}";
        enemyCounter--;
    }

    private void SpawnBoss()
    {
       enemyspawner.SpawnBoss();
    }

    public void Victory()
    {
        gameHasEnded = true;
        victoryMenu.SetActive(true);
        Time.timeScale = 0f;
        endScoreText = scoreText;
    }

    public void Respawn()
    {
        lifePlayer -= 1;
        if (lifePlayer <= 0)
        {
            EndGame();
        }
        player.transform.position = respawnPoint.position;
    }

    public void UpdateLifePlayer()
    {
        pvText.text = "HP : "+ player.GetComponent<PlayerStats>().life;
        lifeText.text = "Remaining Lives : " + lifePlayer;
    }

    #region Menu

    public void Pause()
    {
        _isPaused = true;
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
    }

    public void Resume()
    {
        _isPaused = false;
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
    }

    public void EndGame()
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
            FindObjectOfType<AudioManager>().Stop("Music");
            Debug.Log("Game Over !");
            gameOverMenu.SetActive(true);
            gameOverScoreText = scoreText;
            
            Time.timeScale = 0f;
        }
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }

    #endregion
}
