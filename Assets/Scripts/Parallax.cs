﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] private float _moveSpeed;

    [SerializeField] private float offset;

    private Vector2 startPos;
    private float newYPos;
    
    
    void Start()
    {
        startPos = transform.position;
    }

    
    void Update()
    {
        newYPos = Mathf.Repeat(Time.time * -_moveSpeed, offset);
        transform.position = startPos + Vector2.up * newYPos;
    }
}
