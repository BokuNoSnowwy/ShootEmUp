﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    //La classe qui définit l'objet à pool, son tag, et le nombre d'objets sur le jeu
    #region Pool Class
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject bulletPrefab;
        public int size;
    }
    #endregion
    
    //Un singleton simplifié pour appeler l'ObjectPooler partout
    #region Singleton
    
    public static ObjectPooler Instance;
    
    void Awake()
    {
        Instance = this;
    }

    #endregion
    

    public Dictionary<string, Queue<GameObject>> poolDictionnary;    //On crée un dictionnaire avec un tag, et une queue de GameObject (équivalent d'une liste, mais qui récupère plus rapidement le premier objet)
    public List<Pool> pools;    //On crée une liste du Pool
    
    // Start is called before the first frame update
    void Start()
    {
        poolDictionnary = new Dictionary<string, Queue<GameObject>>();    //On initie le dictionnaire

        foreach (Pool pool in pools)    //Pour chaque objet à pool dans la liste
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.bulletPrefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            
            poolDictionnary.Add(pool.tag, objectPool);    //on ajoute à une Queue l'objet de la liste pools, et on ajoute cette Queue au dictionnaire
        }
    }

    //On fait apparaître l'objet du dictionnaire, avec le tag voulu, à la position voulu. On remet ensuite cet objet dans le dictionnaire, afin de ne jamais avoir à créer ou à supprimer d'objets
    public GameObject SpawnFromPool(string tag, Vector3 spawnPointPos, Quaternion spawnPointRot)
    {
        if (!poolDictionnary.ContainsKey(tag))
        {
            Debug.Log("Pool with tag " + tag + " doesn't exist.");
            return null;
        }
        GameObject objectToSpawn = poolDictionnary[tag].Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = spawnPointPos;
        objectToSpawn.transform.rotation = spawnPointRot;
        
        poolDictionnary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }
}
