﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform bulletSpawnPoint;
    [SerializeField] private bool canShoot;
    [SerializeField] private float timerShoot;
    [SerializeField] private float maxtimerShoot;
    // Start is called before the first frame update
    void Start()
    {
        maxtimerShoot = timerShoot;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetButton("Jump")) && canShoot)
        {
            FindObjectOfType<AudioManager>().Play("Pewpew");
            ObjectPooler.Instance.SpawnFromPool("PlayerBullet", bulletSpawnPoint.position, bulletSpawnPoint.rotation);
            canShoot = false;
        }

        if (!canShoot)
        {
            timerShoot -= Time.deltaTime;
            if (timerShoot <= 0)
            {
                canShoot = true;
                timerShoot = maxtimerShoot;
            }
        }
    }
}
