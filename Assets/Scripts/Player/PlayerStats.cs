﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public float life;
    private float maxLife;
    
    // Start is called before the first frame update
    void Start()
    {
        maxLife = life;
        FindObjectOfType<AudioManager>().Play("Music");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int dmg)
    {
        life -= dmg;
        if (life <= 0)
        {
            Death();
        }
        GameplayManager.Instance.UpdateLifePlayer();
    }

    public void Death()
    {
        FindObjectOfType<AudioManager>().Play("GameOver");
        Debug.Log("death");
        GetComponent<Animator>().SetTrigger("Death");
        life = maxLife;
    }

    public void Respawn()
    {
        GameplayManager.Instance.Respawn();
    }
}
